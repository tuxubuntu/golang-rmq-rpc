package rmq

import "errors"
import "github.com/streadway/amqp"

const MIME = "application/octet-stream"

type callback func([]byte) ([]byte, error)

type Rmq struct {
	conn *amqp.Connection
	ch *amqp.Channel
}

func New(address string) (*Rmq) {
	conn, err := amqp.Dial(address)
	failOnError(err, "Failed to connect to RabbitMQ")
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	err = ch.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
	failOnError(err, "Failed to set QoS")
	self := &Rmq {
		conn: conn,
		ch: ch,
	}
	return self
}

func (self *Rmq) Close() {
	self.ch.Close()
	self.conn.Close()
}

func (self *Rmq) Procedure(name string, cb callback) {
	a := name
	q, err := self.ch.QueueDeclare(
		a,     // name
		false, // durable
		false, // delete when usused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")
	msgs, err := self.ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")
	for d := range msgs {
		res, err := cb(d.Body)
		if err != nil {
			err = self.ch.Publish(
				"",        // exchange
				d.ReplyTo, // routing key
				false,     // mandatory
				false,     // immediate
				amqp.Publishing{
					ContentType:   MIME,
					CorrelationId: d.CorrelationId,
					Type:          "error",
					Body:         []byte(err.Error()),
				},
			)
			failOnError(err, "Failed to publish a message")
		} else {
			err = self.ch.Publish(
				"",        // exchange
				d.ReplyTo, // routing key
				false,     // mandatory
				false,     // immediate
				amqp.Publishing{
					ContentType:   MIME,
					CorrelationId: d.CorrelationId,
					Body:          res,
				},
			)
			failOnError(err, "Failed to publish a message")
		}
		d.Ack(false)
	}
}

func (self *Rmq) Call(name string, body []byte) ([]byte, error) {
	a := name
	q, err := self.ch.QueueDeclare(
		"",  // name
		false, // durable
		false, // delete when unused
		true, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")
	msgs, err := self.ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")
	corrId := randomString(32)
	err = self.ch.Publish(
		"",    // exchange
		a,     // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType:   MIME,
			CorrelationId: corrId,
			ReplyTo:       q.Name,
			Body:          body,
		},
	)
	failOnError(err, "Failed to publish a message")
	for d := range msgs {
		if d.Type == "error" {
			return nil, errors.New(string(d.Body))
		}
		if corrId == d.CorrelationId {
			return d.Body, nil
		}
	}
	return make([]byte, 0), errors.New("Call failed")
}


